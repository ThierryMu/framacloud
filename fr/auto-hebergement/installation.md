Installation de services
========================

Avant d’aller plus loin, précisons quelques conventions
utilisées par la suite.

  * Certaines commandes seront précédées d’un symbole `$`. Cela signifie
    qu’elles ne nécessitent pas les privilèges superutilisateur.
  * Les autres seront précédées par un symbole `#`. Ces
    dernières nécessitent d’avoir les droits d’administrateur pour
    les lancer. Pour devenir superutilsateur, il faut taper la
    commande `su`, puis votre mot de passe (c’est normal s’il ne
    s’affiche pas). On dit alors que vous êtes **root**.
  * Il est possible que, lorsque des commandes seront longues,
    la largeur des pages nous force à passer à la ligne. Bien sûr,
    **il ne faudra pas passer à la ligne** lorsque la commande
    sera tapée. Dans ce document, on utilisera le symbole
    ``\`` avant de faire un faux passage à la ligne.


Les services sont des "démons", autrement dit des programmes qui
tournent en arrière-plan. Notez les points suivants :

  *  Pour arrêter un service, tapez `# service nom-du-service stop`
  *  Pour relancer un service, tapez `# service nom-du-service restart`
  *  Pour démarrer un service, tapez `# service nom-du-service start`
  *  Parfois, on peut recharger la configuration du service sans
    l’arrêter (comme par exemple le serveur http nginx). Pour
    cela, on tape : `# service nom-du-service reload`


La commande `service` est tout à fait équivalente à aller chercher le
script dans /etc/init.d. Par exemple, `# service nginx reload` revient à
`# /etc/init.d/nginx reload`.

La plupart de ces services enregistrent leur activité dans /var/log.

Préparer l’accès ssh
--------------------

SSH, ou Secure Shell , vous permettra principalement d’accéder
à votre serveur à distance de façon sécurisée, et ainsi de
l’administrer.

Pour l’installer, il faut d’ajouter le paquet `openssh-server` sur le
serveur. Sur debian, il faut lancer cette commande :

```
# apt-get install openssh-server
```

Ensuite, il faut :

  1.  Ouvrir le port 22 du parefeu du serveur s’il y en a un.
  2.  Rediriger le port 22 de votre routeur vers le serveur.

Afin de vous connecter en ssh sur votre serveur à partir d’un autre
ordinateur, vous aurez besoin du paquet `openssh-client`. Certains
utilisateurs de windows pourront utiliser le client ssh putty.

La syntaxe pour se connecter au serveur est la suivante :

```
ssh -p PORT UTILISATEUR@SERVEUR
```

Quelques explications :

  *  PORT : il faut préciser le port sur lequel le serveur ssh
        écoute. Par défaut, c’est le 22, mais nous verrons ensuite
        comment le modifier pour plus de sécurité.
  *  UTILISATEUR : sur le serveur, il y a des utilisateurs. Il
        faut simplement préciser le login de l’utilisateur avec lequel
        on souhaite se connecter.
  *  SERVEUR : c’est soit l’adresse ip du serveur, soit le nom de
        domaine.


### Configuration minimale pour la sécurité

SSH permet d’accéder à votre serveur. Il est donc important de veiller
à sa configuration pour éviter des failles de sécurité inattendues.

La configuration du serveur ssh se déroule dans le fichier
`/etc/ssh/sshd_config`. Veillez à préciser **au moins** les
quelques lignes suivantes afin de renforcer la sécurité du serveur :

```
Port 2221
PermitRootLogin no
X11Forwarding no
AllowUsers utilisateur_autorisé_à_se_connecter
```

<div class="alert alert-warning">
<h4>Ça veut dire quoi ces options?</h4>
<ul>
  <li><code>Port 2221</code> : Le port 22 est celui utilisé par défaut pour
        ssh. Les robots ou vilains pirates vont souvent essayer
        d’attaquer par le port 22. On modifie donc le numéro du port
        sur lequel ssh va écouter.
        Mettez ici la valeur que vous souhaitez, en veillant à ouvrir
        ce port dans votre routeur et votre parefeu.</li>
  <li><code>PermitRootLogin no</code> : ssh n’acceptera jamais
        l’utilisateur root (le superman qui peut tout faire sur le
        système). Vous devrez tout d’abord vous connecter en tant que
        simple utilisateurs avant d’obtenir les droits administrateur
        avec la commande "su".</li>
  <li><code>X11Forwarding no</code> : On n’autorise pas le relais vers
        le serveur d’affichage X (les applications graphiques).</li>
  <li><code>AllowUsers</code> : Seuls les utilisateurs listés ensuite
        seront autorisés à se connecter. On peut ici indiquer un
        groupe d’utilisateurs comme <em>sftpusers</em> afin de ne pas
        avoir à éditer ce fichier à chaque nouvel utilisateur.</li>
</ul>
</div>

### Connexion automatique à l’aide de clés

On peut préférer se connecter au serveur à l’aide d’une clé afin de ne
pas avoir à entrer de mot de passe. Voici
rapidement la marche à suivre.

**Notez que “client” désigne l’ordinateur qui veut se connecter au
“serveur”.**


  1. On crée d’une paire de clefs sur le client :
        `ssh-keygen -t rsa -f ~/.ssh/labas`
  2. On configure ssh sur le client : il faut éditer ou créer le
    fichier `~/.ssh/config` et y ajouter :

```
Host labas
HostName nomtreslong.lichtenstein.loin
User bouvardetpecuchet.flaubert
PasswordAuthentication no
IdentityFile ~/.ssh/labas
```

Il faudra bien sûr adapter “HostName” pour qu’il corresponde au nom de
domaine du serveur. De même, modifiez “User” par le nom d’utilisateur se connectant au serveur.

  3.  On configure le serveur : sur le client, exécutez ceci (remplacez “xxx” par le port utilisé par le serveur (22 par défaut))

```
ssh-copy-id -i ~/.ssh/labas.pub -p xxx\
 bouvardetpecuchet.flaubert@nomtreslong.lichtenstein.loin
```
  4. Finalement, vous pouvez vous connecter simplement en tapant
        `ssh labas` . Remplacez "labas" par ce que vous avez
        indiqué à la variable "Host" dans le fichier
        `~/.ssh/config`.



Vos sites web – serveur http
-----------------------------

Un site web est le plus souvent un ensemble de pages .html ou .php. Ce
contenu est fourni aux visiteurs de votre site par un ``serveur
http’’.

Le plus connu des serveurs http est certainement apache. **Ce
n’est pas celui dont on va parler ici**.

En effet, on va lui préférer nginx qui présente des avantages non-négligeables. Il est
beaucoup plus léger, et s’adapte donc mieux à des serveurs aux
configurations modestes, le cas le plus fréquent lorsqu’on
s’auto-héberge.

### Installation de nginx

Comme la plupart du temps sur une debian, l’installation d’un service
est simplissime :

```
# apt-get install nginx
```

Et voilà ! :)

Bon, il y a quand même quelques petites choses à savoir :

  *  Le fichier de configuration global se trouve dans
        ``/etc/nginx/nginx.conf``,
  *  Le dossier ``/etc/nginx/conf.d``, contiendra
        des fichiers se terminant par « .conf ». Chacun de ces
        fichiers permettra de
        configurer un de vos sites.

**Exemple de site** :
Voici un fichier ``/etc/nginx/conf.d/exemple.conf`` qui montre un
exemple de ce à quoi ressemble la configuration d’un site. Remarquez
que les instructions sont dans une section ``server``.

```
server {
listen 80;
server_name monsite.fr;
index index.html;
root /media/www/monsite;
access_log /var/log/nginx/monsite.log;

    location ~*  \.(jpg|jpeg|png|gif|ico|svg|mp4|ogg|ogv|webm|css|js)$ {
    expires 1M;
    }
}
```

Dans ce cas, toutes les pages html déposées dans /media/www/monsite
seront disponibles à l’adresse http://monsite.fr .

**Pour utiliser php**, il sera nécessaire d’ajouter quelques
lignes au fichier ci-dessus :

```
server {
listen 80;
server_name monsite.fr;
index index.html;
root /media/www/monsite;
access_log /var/log/nginx/monsite.log;

    location ~*  \.(jpg|jpeg|png|gif|ico|svg|mp4|ogg|ogv|webm|css|js)$ {
    expires 1M;
    }

    location ~ \.php$ {
        try_files $uri = 404;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        include fastcgi_params;
        fastcgi_intercept_errors on;
        fastcgi_param HTTPS on;
        fastcgi_param   SCRIPT_FILENAME  $document_root$fastcgi_script_name;
    }
}
```

Et bien sûr, il faudra installer php :

```
# apt-get install php5-fpm php5-apcu
```

<div class="alert alert-warning">
    <h4>Tu nous balances pleins de lignes bizarres, mais moi,
    j’ai envie de savoir ce que ça veut dire!</h4>
    <p>Voyons rapidement ce que signifient les lignes dans ces fichiers de
    configuration :</p>
    <ul>
        <li><code>listen 80;</code> : Le site est accessible sur le port 80
        <li><code>server_name</code> : Le domaine du site est
            monsite.fr . Ça permet à nginx de donner le bon site si, sur le
            même serveur, on a plusieurs sous-domaines.
            Par exemple, un
            site en blog.monsite.fr, un autre en webmail.monsite.fr… </li>
        <li><code>index index.html;</code> : Si le visiteur ne précise pas de
            page html , on cherche le fichier index.html. Exemple :
            monsite.fr/ est équivalent à monsite.fr/index.html</li>
        <li><code>root /media/www/monsite;</code> : L’emplacement où se
            trouvent les fichiers du site sur le serveur
        <li><code>access_log</code> : L’emplacement des logs d’accès au
            site</li>
        <li><code>location …</code> : Cette instruction permet de
            réaliser certaines actions selon ce que le visiteur demande au
            serveur. Ici, on augmente le cache pour les fichiers de type
            image. Plus bas, on traite les fichiers en php de façon à ce
            qu’ils soient rendus par le serveur. Pour plus de détails sur
            ce sujet, il est conseillé de visiter le site de
            <a href="http://nginx.org/en/docs/http/ngx_http_core_module.html">nginx</a>.</li>
    </ul>
</div>

Courrier électronique
---------------------

Cette partie explique l’installation d’un serveur mail le plus simple
possible. Aucune base de donnée ne sera utilisée, car ça n’a aucune
intérêt en auto-hébergement.

L’installation va se dérouler en quelques étapes :

  1. Configuration d’enregistrement DNS particuliers[^1] et préparation du serveur,
  2. Création des certificats pour sécuriser l’authentification
        avec le serveur
  3. Configuration d’opensmtpd, qui se chargera d’envoyer et
        recevoir votre courrier,
  4. Configuration de dovecot, qui permet la réception du
        courrier avec un client comme thunderbird,
  5. Faire le nécessaire pour que vos messages ne soient pas
        considérés comme spam.
  6. Installer un antispam

On verra ensuite comment ajouter de nouveaux comptes mail sur votre
serveur, et comment configurer votre client de messagerie
(Thunderbird) pour l’utiliser.

### Configuration des DNS pour le courrier électronique et préparation du serveur

Chez votre registrar[^1], ajoutez deux nouveaux
champs :

*  Un champ de type A qui pointe vers votre IP :

        mail.votredomaine.net A 109.190.193.182

   Bien sûr, remplacez ``109.190.193.182`` par votre IP.
   
*  Un champ de type MX qui pointe vers le A précédent

        votredomaine.net. MX 1 mail.votredomaine.net.

Sur votre serveur, modifiez le fichier ``/etc/hosts``
pour rajouter une ligne de ce type :

```
127.0.0.1       mail.votredomaine.net
```

Enfin, ouvrez et redirigez les ports suivants : TCP 25, 143, 587 et 993[^3].

On peut maintenant passer à l’installation du serveur mail.

### Création des certificats

Nous avons ces deux commandes à lancer :

```
openssl genrsa -out /etc/ssl/private/mail.votreserveur.net.key 4096
openssl req -new -x509 -key /etc/ssl/private/mail.votreserveur.net.key -out /etc/ssl/certs/mail.votreserveur.net.crt -days 730
```

On modifie les droits de lecture sur ces deux fichiers par sécurité :

```
chmod 640 /etc/ssl/private/mail.votreserveur.net.key
chmod 640 /etc/ssl/certs/mail.votreserveur.net.crt
```

C’est tout :) !

PS: autant opensmtpd que dovecot peuvent utiliser les certificats fournis par Let's Encrypt. Pour générer ces certificats, 
voir le chapitre sur [les services web](services_web.html#obtenir-un-certificat-avec-letsencrypt). Dans un premier temps, il est 
tout de même plus simple de valider la configuration avec un certificat généré localement...

### Opensmtpd

Hop, on installe opensmtpd à la manière debian.

```
# apt-get install opensmtpd opensmtpd-extras
```

Pour configurer opensmtpd, on va mettre le contenu suivant dans le
fichier ``/etc/smtpd.conf``.

```
table aliases file:/etc/aliases

pki mail.votreserveur.net key "/etc/ssl/private/mail.votreserveur.net.key"
pki mail.votreserveur.net certificate "/etc/ssl/certs/mail.votreserveur.net.crt"

# Deliver
listen on lo
listen on eth0 port 25  hostname mail.votreserveur.net tls pki mail.votreserveur.net
listen on eth0 port 587 hostname mail.votreserveur.net tls-require pki mail.votreserveur.net auth

accept from local for local  alias <aliases> deliver to maildir "~/Maildir"
accept from any for domain "votreserveur.net" deliver to maildir "~/Maildir"

# Relay
accept from local for any relay hostname mail.votreserveur.net
```

Vous n’avez quasiment rien à modifier dans ce fichier
(``/etc/smtpd.conf``), mis à part
remplacer ``votreserveur.net`` par votre nom de domaine.

Pour terminer, ouvrez les ports 587 et 25 dans votre parefeu et votre
routeur.
Voilà pour opensmtpd.

### Dovecot

Dovecot va être utilisé comme serveur imap, afin de pouvoir récupérer
son courrier à partir d’un client comme thunderbird.

On installe dovecot :

```
# apt-get install dovecot-imapd
```

Ajoutez maintenant ces lignes à la fin du fichier
``/etc/dovecot/dovecot.conf``

```
# listen both ipv4 and ipv6
listen = *, ::

# we use maildir
mail_location = maildir:~/Maildir

# imap > pop
protocols = imap

# no plaintext, let’s keep secure with ssl
ssl = yes
ssl_cert = </etc/ssl/certs/mail.votreserveur.net.crt
ssl_key = </etc/ssl/private/mail.votreserveur.net.key
disable_plaintext_auth = yes

passdb {
        driver = pam
            args = failure_show_msg=yes
        }

userdb {
        driver = passwd
    }

```

Pour terminer cette partie, on peut relancer les services qui sont
maintenant configurés :

```
# service opensmtpd restart
# service dovecot restart
```

<div class="alert alert-warning">
    <h4>Vérifier que les processus fonctionnent correctement</h4>
    <p>Opensmtpd a la facheuse tendance à ne pas signaler quand il ne démarre pas correctement.  Pour s'assurer que tous les processus sont démarrés, deux solutions:</p>
    <ul>
       <li><code>service opensmtpd status</code></li>
       <li><code>ps -ef |grep smtpd</code></li>
    </ul>
</div>

### Ne pas être mis dans les spams

Plusieurs dispositifs permettent de montrer que les mails sortant de
votre serveur ne sont pas des spams. Prenons-les un par un.

**SPF** :
Ajoutez un champ DNS[^1]  de type SPF auprès de votre registrar tel que celui-ci :

    votredomaine.net.   SPF ``v=spf1 a mx ~all’’

ou bien sous forme de champ TXT :

    votredomaine.net. TXT ``v=spf1 a mx ~all’’

**DKIM** (facultatif) :
On va ici ajouter une signature cryptographique aux messages. Cette signature sera
vérifiée à partir d’une indication que l’on rajoutera un peu plus tard
à nos champs DNS.

Pour l’instant, on installe dkimproxy comme d’habitude :

```
# apt-get install dkimproxy
```

Il faut maintenant permettre à dkimproxy de lire les clés qui
permettront de signer nos messages :


```
chown root:dkimproxy /etc/ssl/private/mail.votreserveur.net.key
chown root:dkimproxy /etc/ssl/certs/mail.votreserveur.net.crt
```

Sous debian, il nous reste un peu de travail. Nous allons éditer le
fichier ``/etc/default/dkimproxy`` pour décommenter (retirer le
``\#``) de ces lignes :

```
RUN_DKIMPROXY_OUT=1
DKIMPROXY_OUT_CONF="/etc/dkimproxy/dkimproxy_out.conf"
DKIMPROXY_OUT_PRIVKEY="/etc/ssl/private/mail.votreserveur.net.key"
DKIM_HOSTNAME="votreserveur.net"
DOMAIN="votreserveur.net"
```

Maintenant, c’est le ficher ``/etc/dkimproxy/dkimproxy\_out.conf``
qu’il faut modifier :

```
listen    127.0.0.1:10028
relay     127.0.0.1:10029
domain    votredomaine.net
signature dkim(c=relaxed)
signature domainkeys(c=nofws)
keyfile   /etc/ssl/private/mail.votreserveur.net.key
selector  pubkey
```

Bien, reste à indiquer à opensmtpd de signer les mails. Le fichier
``/etc/smtpd.conf`` ressemble désormais à ça :

```
table aliases file:/etc/aliases

pki mail.votreserveur.net key "/etc/ssl/private/mail.votreserveur.net.key"
pki mail.votreserveur.net certificate "/etc/ssl/certs/mail.votreserveur.net.crt"

# Deliver
listen on lo
listen on lo port 10029 tag DKIM
listen on eth0 port 25  hostname mail.votreserveur.net tls pki mail.votreserveur.net
listen on eth0 port 587 hostname mail.votreserveur.net tls-require pki mail.votreserveur.net auth

accept from local for local  alias <aliases> deliver to maildir "~/Maildir"
#accept from any for domain "votreserveur.net" deliver to maildir "~/Maildir"

# Relay
# dkim tagged can be sent
accept tagged DKIM for any relay hostname mail.votreserveur.net
# if not dkim tagged, send it to dkimproxy
accept from local for any relay via smtp://127.0.0.1:10028 hostname mail.votreserveur.net
```

Finalement, il faut ajouter un nouveau champ dans vos
DNS[^1] auprès de
votre registrar. Eh oui, encore! On va en réalité indiquer le
nécessaire pour pouvoir vérifier la signature des messages qui auront
un « drapeau » « pubkey ».

Il s’agira d’un champ DKIM ou TXT selon ce qui est disponible.
Remplissez-le ainsi :

  *  Nom de domaine : `pubkey._domainkey.votredomaine.net`
  *  Contenu : `v=DKIM1; k=rsa; p=.....`
    Recopiez en fait dans le contenu ce qui est dans le fichier
    ``/etc/ssl/certs/mail.votreserveur.net.crt``

<div class="alert alert-warning">
    <h4>Longueur du champ DNS TXT</h4>
    <p>Le contenu d'un champ TXT DNS ne peut dépasser 255 caractères, sous
    peine d'erreur.</p>
    <p>Comme la clé utilisée pour la signature est souvent de 2048 bits ou plus, 
    cela pose problème.  La solution est de couper la clé en plusieurs chaînes de 
    caractères dans le DNS, comme dans l'exemple ci-dessous:</p>
   
    <code>pubkey._domainkey.votredomaine.net IN TXT "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwP5oG43C/yrFwUvVKQdm1ILpU13QpKldokrTlV59sBp0uqltMDszrSeeLzBMioTx2M7/fodLP1+Iad5ZgwSmnxg/aXys73y9UEz8dAYVCOnC3En+34b1CGjMhp/ph2UISnnMSmAOV6sbiyeZ4Rk4KDjjr8rj5sx9uHmxoDqOpFHu+PRqLlRjKQVu2K3O" "8zMyyf2HB7z+1cHSwMJPd6B0T5V7hZto8Hyv6vNVDzFs8v/Flnp7DqsdVnFT62WMMqIFJTNIzbgAMQFL4+vKUYxFc5Wti8V5o9fF9utPcb/+HWZzpyveJ1pNRDnmj0EIlcO8SMfq8Sr7F/gtoDKEs0nKNKQIDAQAB"</code>

</div>

Pour être tout à fait complet, un dernier champ DNS peut être rajouté: il s'agit 
d'un champ DMARC ("Domain-based Message Authentication, Reporting and Conformance"):

``` 
_dmarc IN TXT    "v=DMARC1; p=none;"
```


Enfin, il faut relancer les services impactés par les modifications:

``` 
# service dkimproxy restart
# service opensmtpd restart
``` 

**Suis-je considéré comme spam ?**
Pour vérifier que vous n’êtes pas en spam, suivez les indications de
ce site <https://www.mail-tester.com>.


### Installer un antispam

On installe l’antispam et le démon qui va filtrer les mails reçus :

```
# apt-get install spamd spamassassin razor
```

On modifie la configuration de spampd dans le fichier
``/etc/default/spampd``

```
STARTSPAMPD=1
# The IP to listen on
LISTENHOST=127.0.0.1
# The port to listen on
LISTENPORT=10035
# The host to forward the connection to
DESTHOST=127.0.0.1
# The port to forward the connection to
DESTPORT=10036
```

On active razor qui est un outil collaboratif de détection de spams et qui va
être utilisé par spamassassin:

```
razor-admin --create
razor-admin --register
```

Ensuite, on active spamassassin :

```
systemctl enable spamassassin.service
```

Enfin, on modifie la configuration d’opensmtpd dans le fichier
``/etc/smtpd.conf``, qui avec la configuration dkim donne :

```
table aliases file:/etc/aliases

pki mail.votreserveur.net key "/etc/ssl/private/mail.votreserveur.net.key"
pki mail.votreserveur.net certificate "/etc/ssl/certs/mail.votreserveur.net.crt"

# Deliver
listen on lo
listen on lo port 10029 tag DKIM
listen on lo port 10036 tag ANTISPAM
listen on eth0 port 25  hostname mail.votreserveur.net tls pki mail.votreserveur.net
listen on eth0 port 587 hostname mail.votreserveur.net tls-require pki mail.votreserveur.net auth

accept tagged ANTISPAM for any alias <aliases> deliver to maildir "~/Maildir"
accept from local for local  alias <aliases> deliver to maildir "~/Maildir"
#accept from any for domain "votreserveur.net" deliver to maildir "~/Maildir"

# Relay
#accept from local for any relay # not filter

# antispam
accept from any for domain "votreserveur.net" relay via "smtp://127.0.0.1:10035"

# dkim tagged can be sent
accept tagged DKIM for any relay hostname mail.votreserveur.net
# if not dkim tagged, send it to dkimproxy
accept from local for any relay via smtp://127.0.0.1:10028 hostname mail.votreserveur.net
```

### Ajouter un nouveau compte mail

Un compte mail est en fait un simple compte d’utilisateur. Vous pouvez
donc en créer un nouveau ainsi :

```
# adduser --shell /bin/false nouvel_utilisateur
```

Et voilà ! :)

<div class="alert alert-warning">
<p>Notez que l’on précise le shell qui sera celui du nouvel
    utilisateur. Ce n’est pas n’importe lequel, puisqu’il s’agit de
    <code>/bin/false</code>. Cela permet de s’assurer que l’utilisateur
    ne pourra lancer aucune commande sur le serveur.</p>
<p>C’est un plus pour la sécurité.</p>
</div>

### Configurer son client de messagerie

Pour consulter vos mails sur le serveur, vous pouvez utiliser un
client de messagerie comme l’excellent [thunderbird](https://www.mozilla.org/fr/thunderbird/),
logiciel-libre respectueux de votre vie privée.

Voici les paramètres qu’il faudra indiquer au logiciel pour envoyer et
recevoir des courriels. Notez que tous ne vous seront peut-être pas
demandés :

  * Nom du serveur : mail.votredomaine.net
  * Adresse électronique : votre_identifiant@votredomaine.net
  * Nom d’utilisateur : l’identifiant choisi à la création d’un
        compte mail (voir [Ajouter un nouveau compte mail](#ajouter-un-nouveau-compte-mail))
  * Serveur entrant : IMAP 
    *  port : 143 (ou 993)
    *  SSL : STARTTLS (ou SSL/TLS)
  * Serveur sortant : SMTP
    *  port : 587
    *  SSL : STARTTLS

Vous souhaiterez peut-être plutôt utiliser un webmail, afin d’accéder
à votre messagerie à partir d’un navigateur web. Cela est expliqué au
paragraphe [Webmail](services_web.html#webmail).

### Redirection de mail

Il est simplissime de rediriger les mails reçus sur une adresse vers
un autre compte de messagerie.

Par exemple, vous disposez d’une adresse
bibi@mondomaine.com et souhaitez que tous les messages que bibi reçoit
soient transférés automatiquement à bibitheboss@openmailbox.org.

Pour ça, il suffit d’éditer le fichier ``/etc/aliases``, puis d’y
ajouter une ligne :

```
bibi: bibitheboss@openmailbox.org
```

De façon générale, ça donne :

```
utilisateur: adresse1.mail.com, adresse2.mail.com
```

Afin que ce changement soit pris en compte, il reste une dernière
étape : lancez la commande

```
newaliases
```

Et voilà :)

Serveur sftp
------------

Sftp, ou « Secure File Transfert Protocol » est une variante du
protocole FTP nettement plus sécurisée puisqu’elle passe par un tunnel
ssh.

Suite à cette installation, vous pourrez partager des fichiers avec
ceux que vous voulez, en toute sécurité. Eh oui, c’est ssh qui s’en
charge! Il sera possible de déposer des fichiers sur le serveur, et
d’en télécharger.

Dans la suite, on suppose que les fichiers déposés sur le serveur sont
stockés dans ``/media/sftp``

### Configuration du serveur sftp

Un serveur sftp est disponible lorsqu’on installe le paquet
`openssh-server`. Donc si vous avez déjà un accès ssh sur votre
serveur, c’est en fait prêt. Enfin presque.

En effet, il faut modifier la configuration de ssh. Souvenez-vous,
c’est dans le fichier ``/etc/ssh/sshd\_config`` :

```
Match Group sftpusers
ChrootDirectory /media/sftp
ForceCommand internal-sftp -f LOCAL7 -l INFO
AllowTcpForwarding no
```

<div class="alert alert-info">
<p>Tous les utilisateurs du groupe sftpusers seront coincés dans
le dossier <code>/media/sftp</code>.</p>
<p>Utiliser un groupe sftpusers exprès
    pour l’accès en sftp permet de facilement rajouter des utilisateurs,
    tout en gardant le tout sécurisé.</p>
<p>On appelle cela faire un chroot</p>
</div>

Le dossier de stockage ``/media/sftp`` doit appartenir à root pour plus de sécurité. Lancez
la commande suivante pour que l’on ne puisse pas remonter plus haut
dans l’arborescence :

```
# chown root:root /media/sftp
```

Créez un dossier ``/home`` dans le dossier ``/media/sftp`` pour que les
utilisateurs y soient automatiquement placés à leur connexion.

```
# mkdir -p /media/sftp/home
```

Dans ``/media/sftp/home``, il y aura les dossiers portant le nom
des utilisateurs.

<p class="alert alert-info">
En fait, on fait comme si le répertoire /media/sftp était la
nouvelle racine / pour les utilisateurs.</p>


### Ajouter un compte sftp

Ajouter un compte sftp revient à créer un nouvel
utilisateur et mettre ce dernier dans le groupe sftpusers.

Il faut quand même faire attention à plusieurs points :

  *  L’utilisateur ne doit pouvoir faire QUE du sftp, on va donc
        changer son shell
  *  L’utilisateur n’a pas besoin de dossier personnel dans
        ``/home``, mais aura besoin de son dossier dans
        ``/media/sftp``
  *  Il faut s’assurer que seul l’utilisateur a des droits
        d’écriture dans son dossier.


Je vous propose donc un script ci-dessous qui explique la mise en
place d’un nouvel utilisateur en faisant attention aux points
ci-dessus :


```
#!/bin/sh
# $1 : username
# $2 : hostname
if [ $# -ne 2 ]; then
    echo "need two argument"
    exit 1
fi

### Création d’un utilisateur, qui n’a pas de shell
# et ne peut donc nuire au serveur
# On ne lui crée pas de /home/utilisateur non plus.
adduser --shell /bin/false --no-create-home "$1"

### On ajoute l’utilisateur au groupe sftpusers
usermod -a -G sftpusers "$1"

### On crée le répertoire utilisateur dans le chroot
mkdir -p "/media/sftp/home/$1"

### Accès seulement (pas d’ecriture) au dossier de l’utilisateur
chmod 555 "/media/sftp/home/$1"

### Répertoire seulement accessible en lecture/écriture
# par l’utlisateur
mkdir -p "/media/sftp/home/$1/prive"
chmod 700 "/media/sftp/home/$1/prive"

### On rend l’utilisateur propriétaire de son répertoire
chown -R $1:$1 "/media/sftp/home/$1"

# ajout de l’utilisateur pour sshd_config
sed -i "s/AllowUsers.*/& $1/" /etc/ssh/sshd_config

service ssh restart

exit 0
```

### Utiliser le serveur sftp

Les utilisateurs appartenant au groupe *sftpusers* peuvent
télécharger des fichiers sur le serveur (ou en récupérer) avec un client sftp. Par
exemple, [Filezilla](https://filezilla-project.org/) est un client multi plateforme qui supporte le
protocole sftp.

### Accéder aux fichiers via un navigateur web

Afin de rendre les fichiers déposés par les utilisateurs disponibles
au public, ajoutez ces lignes dans la configuration de nginx :

```
location ~ ^/~(.+?)(/.*)?$ {
    alias /media/sftp/home/$1/$2;
    index  index.html index.htm;
    autoindex on;
}
```

Les documents seront accessibles à l’adresse
<http://votreserveur.com/~utilisateur>, ce qui est plus pratique que de
devoir télécharger un client sftp pour un utilisateur moins averti.

Pour qu’un dossier ne soit pas accessible sur le net (en http)
(utilisateur privé), il suffit de changer les droits sur son dossier
avec la commande :

```
chmod 700 /media/home/sftp/home/utilisateur
```

Messagerie instantanée
----------------------

Pour la messagerie instantanée, quoi de mieux qu’un serveur xmpp ?
C’est un protocole libre qui permet à la fois la communication en
texte, mais aussi en audio et vidéo, ainsi que d’échanger des fichiers et bien
plus.

Le programme qui fera office de serveur [xmpp](https://xmpp.org/) sera
[Prosody](https://prosody.im/), qui est simple à configurer et très léger.

### Installation

L’installation se réalise sans douleurs avec apt :

```
# apt-get install prosody liblua5.1-sec
```

Ouvrez ensuite les ports[^3] 5222 et 5269 en
TCP dans votre routeur et votre parefeu.

Enfin, ajoutez deux champs DNS[^1] chez votre
registrar :

  *  Champ de type A : xmpp.domaine.net
  *  champs de type SRV vers xmpp.domaine.net :
    * `_xmpp-client._tcp.domaine.net. 18000 IN SRV 0 5 5222 xmpp.domaine.net.`
    * `_xmpp-server._tcp.domaine.net. 18000 IN SRV 0 5 5269 xmpp.domaine.net.`


On peut maintenant passer à la configuration du serveur xmpp.

### Configuration

La configuration de prosody se passe dans le fichier
``/etc/prosody/prosody.cfg.lua``. Il s’agit d’un fichier écrit en
langage lua. Pour décommenter une ligne, il faudra retirer les double
tirets `--` en début de ligne pour par exemple activer un module.

Par défaut, l’inscription au serveur avec un client (comme Gajim) est
désactivée. Pour l’activer, changez cette ligne en :

```
allow_registration = true;
```

Pour votre serveur, indiquez le nom de domaine que vous avez créé dans
vos DNS pour la communication xmpp :

```
VirtualHost "xmpp.votreDomaine.net"
```

Tout ce que l’on écrira en dessous cette ligne sera la configuration
pour ce domaine uniquement.

En l’occurence, on va activer ce domaine :

```
enabled = true;
```

Enfin, redémarrez prosody pour pouvoir l’utiliser:

```
service prosody restart
```

Vous pouvez maintenant commencer à discuter avec votre client xmpp
favori (gajim, psi, jitsi…).

### Ajouter un nouveau compte xmpp

Pour ajouter un nouveau compte de messagerie sur votre serveur, vous
pouvez utiliser un client comme [Gajim](https://gajim.org/) à
condition d’avoir autorisé les inscriptions.

Sinon, la commande suivante permet d’ajouter sur votre serveur un
nouvel utilisateur :

```
# prosodyctl adduser utilisateur@xmpp.votreDomaine.net
```

### Utiliser une connexion SSL

On se place dans /etc/prosody/certs, puis on lance les commandes :

```
# openssl req -new -x509 -nodes \
    -out votreDomaine.cert -keyout votreDomaine.key
# chown prosody:prosody votreDomaine.*
```

Ou alors, allez dans le dossier /etc/prosody/certs, vous y trouverez
un fichier Makefile contenant des explications pour créer facilement
des certificats en utilisant juste make.

Ensuite, précisez bien les certificats dans le fichier de configuration de prosody :

```
ssl = {
    key = "/etc/prosody/certs/votreDomaine.key";
    certificate = "/etc/prosody/certs/votreDomaine.cert";
}
```

Vous pouvez exiger le chiffrement en décommentant cette ligne :

```
c2s_require_encryption = true
```

Une fois les modifications effectuées, pensez à redémarrer prosody :

```
service prosody restart
```

### Ajouter des modules

Prosody propose de nombreux modules. Pour les activer, ajouter leurs
noms dans la section `modules_enabled = {`. Vous trouverez la liste des
modules disponibles dans /usr/lib/prosody/modules. Remarquez que
les modules ont un nom commençant par « mod\_ ». Vous n’avez pas à
préciser cette partie lorsque vous chargez un module.

### Salons de discussion

Vous pouvez héberger des salons de discussion facilement avec prosody en
indiquant dans le fichier de configuration :

```
Component "conf.votreDomaine.net" "muc"
```

Seedbox
-------

Une seedbox est en fait un serveur qui partage en continu les
torrents. De quoi télécharger ses distributions linux favorites à toutes
heures :)

### Installation de transmission

Tout d’abord, installez le paquet transmission-daemon :

```
# apt-get install transmission-daemon
```

Ensuite, éditez le fichier ``/etc/transmission-daemon/settings.json``

Une fois vos modifications effectuées, relancez transmission avec la
commande

```
# service transmission-daemon reload
```

### Ajout automatique des torrents dans un dossier

Pour qu’un torrent soit ajouté en téléchargement dès que vous  le copiez dans un dossier, vous devez modifier le fichier
``/etc/transmission-daemon/settings.json``, pour y mettre ces
deux lignes :

```
"watch-dir": "/chemin/vers/le/dossier/qui/contient/les/torrents",
"watch-dir-enabled": true
```

### Accéder à l’interface web avec un navigateur

Vous pouvez contrôler transmission par le biais d’un navigateur.

![](images/transmission.png)

La suite suppose que vous avez déjà installé et configuré nginx[^2]
En plus, cet accès peut être limité par mot de passe!
Dans le fichier ``/etc/transmission-daemon/settings.json``,
modifiez ces lignes à votre convenance :

```
"rpc-authentication-required": true,
"rpc-bind-address": "0.0.0.0",
"rpc-enabled": true,
"rpc-password": motdepassedelamort,
"rpc-port": 9091,
"rpc-url": "/transmission/",
"rpc-username": "nomdutilisateur",
"rpc-whitelist": "127.0.0.1",
"rpc-whitelist-enabled": true,
```

Une fois vos modifications terminées, rechargez la configuration de
transmission. **Attention, avec « reload » et pas « restart »**

```
# service transmission-daemon reload
```

Et pour la configuration de nginx, copiez ce qui suit dans par exemple
``/etc/nginx/conf.d/transmission.conf``

```
server {
    listen 443 ssl;
    server_name votredomaine.com;

    location / {
        proxy_pass_header X-Transmission-Session-Id;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://127.0.0.1:9091/transmission/web/;
    }

    # Also Transmission specific
    location /rpc {
        proxy_pass         http://127.0.0.1:9091/transmission/rpc;
    }
}
```

Relais TOR
----------

[TOR](https://www.torproject.org/) est un logiciel libre
permettant de passer outre les surveillances subies lors de
l’utilisation d’internet. Lorsqu’on
l’utilise, les communications sont réparties à travers une maille de
serveurs, afin d’obtenir un onion router. En gros, ce que vous
demandez sur internet circule entre pleins de serveurs (les
couches de l’oignon), ce qui rend très difficile de savoir d’où
viennent les paquets, et donc de vous localiser !

![](images/onion.png)

Il vous est cependant possible de participer à ce réseau en étant un
serveur relais. Qui plus est, cela rendra d’autant plus anonyme vos
propres activités, puisqu’il sera difficile de repérer vos centres
d’intérêt parmi le trafic sortant de votre connexion.

Après avoir installé le paquet ``tor``, voici comment configurer un
relais.

Assurez-vous d’ouvrir dans votre pare-feu, ainsi que dans
votre routeur[^3], le port 9001.

Ensuite, éditez le fichier ``/etc/tor/torrc``, afin d’obtenir ces quelques lignes :

```
ORPort 9001
Nickname Surnom
RelayBandwidthRate 50 KB
RelayBandwidthBurst 100 KB
ContactInfo votrenom <adresse AT email dot fr>
ExitPolicy reject *:* # no exits allowed
```

Augmentez les valeurs pour `RelayBandwidthRate` et
`RelayBandwidthBurst` selon votre connexion internet. C’est la bande
passante que vous laissez disponible pour tor.

Enfin, lorsque vous redémarrez tor avec `/etc/init.d/tor restart`,
vous devez voir apparaître dans le fichier de log
``/var/log/tor/log`` :

    ``
    Aug 28 14:09:08.000 [notice] Self-testing indicates your ORPort is reachable from the outside. Excellent. Publishing server descriptor. Aug 28 14:09:12.000 [notice] Performing bandwidth self-test...done.

[^1]: voir le chapitre sur les [noms de domaine](prerequis.html#un-nom-de-domaine)
[^2]: voir la section sur [nginx](#installation-de-nginx)
[^3]: voir la section sur [la redirection des ports](prerequis.html#apprenez-%C3%A0-rediriger-les-ports-de-votre-box)
