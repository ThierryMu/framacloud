# Installation de Searx

[Framabee][1] est le méta-moteur de recherche proposé par Framasoft basé
sur le logiciel [searx][2]. Voici un tutoriel pour vous aider à
l’installer sur votre serveur. Il est conçu pour une installation
pas à pas pour une Debian (testée) ou une Ubuntu (non testée) avec [virtualenv][3].
Il s'appuie sur les explications fournies sur le site du [logiciel][4]

## Installation

### 1 - Préparer la terre

![](images/icons/preparer.png)

Pour Ubuntu, assurez-vous d'avoir activé les dépôts universe (voir
<http://doc.ubuntu-fr.org/depots#depots_officiels>).
### 2 - Semer

![](images/icons/semer.png)

Pour une installation basique

#### Installation des dépendances systèmes nécessaires

    sudo apt-get install git build-essential libxslt-dev python-dev \
    python-virtualenv python-pybabel zlib1g-dev \
    libffi-dev libssl-dev

#### Installation de searx

    cd /usr/local
    sudo git clone https://github.com/asciimoo/searx.git
    sudo useradd searx -d /usr/local/searx
    sudo chown searx:searx -R /usr/local/searx

#### Installation des dépendances python dans virtualenv

    sudo -u searx -i
    cd /usr/local/searx
    virtualenv searx-ve
    . ./searx-ve/bin/activate
    pip install -r requirements.txt
    python setup.py install

### 3 - Arroser

![](images/icons/arroser.png)

#### Configuration

Modifiez la phrase secrète du fichier

`searx/settings.yml` (ceci est une étape nécessaire pour la sécurité de votre application)

    sed -i -e "s/ultrasecretkey/`openssl rand -hex 16`/g" searx/settings.yml

Ouvrez `searx/settings.yml` et baladez-vous dans les différentes options de configuration.
Modifiez-les selon vos besoins et vos envies.

#### Vérification

Il est ici nécessaire d'être dans le virtualenv. Si vous n'y êtes plus :

<pre>sudo -u searx -i
cd /usr/local/searx
. ./searx-ve/bin/activate</pre>

Lancez searx :

<pre>python searx/webapp.py</pre>

Vérifiez le fonctionnement de searx en vous rendant à l'adresse http://localhost:8888.
Si tout fonctionne correctement, coupez le serveur de test (Ctrl+C) et
désactivez l'option de debug dans settings.yml :

<pre>sed -i -e "s/debug : True/debug : False/g" searx/settings.yml</pre>

Searx n'est pas dæmonisé (il ne tourne pas en arrière-plan).
Cette dæmonisation est le rôle d'uwsgi. Vous pouvez maintenant sortir du
virtualenv (il suffit d'utiliser la commande `deactivate`) et de
l'utilisateur searx. Exécutez la commande exit pour cela.

#### uwsgi

Installation des paquets :

<pre>sudo apt-get install uwsgi uwsgi-plugin-python</pre> Créez le fichier de configuration /etc/uwsgi/apps-available/searx.ini avec ce contenu :

<pre>[uwsgi]
# Quel est l'utilisateur qui fera tourner le code
uid = searx
gid = searx

# Nombre de workers (habituellement, on met le nombre de processeurs de la machine)
workers = 4

# Quels sont les droits sur le socket créé
chmod-socket = 666

# Plugin à utiliser et configuration de l'interpréteur
single-interpreter = true
master = true
plugin = python

# Module à importer
module = searx.webapp

# Chemin du virtualenv
virtualenv = /usr/local/searx/searx-ve/
</pre>

Activez la configuration de l'application et relancez uwsgi

<pre>cd /etc/uwsgi/apps-enabled
sudo ln -s ../apps-available/searx.ini
sudo /etc/init.d/uwsgi restart</pre>

#### Serveur web

##### Nginx

Si nginx n'est pas installé (uwsgi ne fonctionnera pas avec le paquet nginx-light) :

<pre>sudo apt-get install nginx</pre>

Créez le fichier de configuration `/etc/nginx/sites-available/searx`
avec ce contenu :

##### Hébergé à la racine (/)

<pre>server {
    listen 80;
    server_name searx.example.com;
    root /usr/local/searx;

    location / {
        include uwsgi_params;
        uwsgi_pass unix:/run/uwsgi/app/searx/socket;
    }
}</pre>

Activez la configuration dans nginx :

<pre>sudo ln -s /etc/nginx/sites-available/searx /etc/nginx/sites-enabled/</pre>

Vérifiez la configuration de nginx :

<pre>sudo nginx -t</pre>

Relancez nginx et uwsgi :

<pre>sudo service nginx restart
sudo service uwsgi restart</pre>

##### Hébergé dans un sous-répertoire de l'URL (/searx)

<pre>location = /searx { rewrite ^ /searx/; }
location /searx {
    try_files $uri @searx;
}
location @searx {
    uwsgi_param SCRIPT_NAME /searx;
    include uwsgi_params;
    uwsgi_modifier1 30;
    uwsgi_pass unix:/run/uwsgi/app/searx/socket;
}</pre>

Activez l'option base_url dans `searx/settings.yml` :

<pre>base_url : http://your.domain.tld/searx/</pre>

Relancez nginx et uwsgi :

<pre>sudo service nginx restart
sudo service uwsgi restart</pre>

##### Apache (non testé)

Ajoutez le module uwsgi :

<pre>sudo apt-get install libapache2-mod-uwsgi
sudo a2enmod uwsgi</pre>

Ajoutez cette configuration :

<pre>Options FollowSymLinks Indexes
SetHandler uwsgi-handler
uWSGISocket /run/uwsgi/app/searx/socket</pre>

Relancez Apache :

<pre>sudo /etc/init.d/apache2 restart</pre>

## Personnalisation

Si vous souhaitez modifier Searx, effectuez vos changements, puis placez-vous dans le virtualenv :

<pre>sudo -u searx -i
cd /usr/local/searx
virtualenv searx-ve
. ./searx-ve/bin/activate</pre>

Relancez l'installation pour que vos modifications soient prises en compte :

<pre>python setup.py install</pre>

Si vous avez ajouté des fichiers statiques ou des templates,
il est possible que cette étape échoue. Ouvrez `setup.py` et regardez
la section `package_data`.
Il faut absolument que seuls des fichiers correspondent aux chemins de cette section.
Par exemple, `static/default/css` ne contient que des fichiers.
Si vous avez ajouté un dossier dans `static/default/css`,
le chemin `static/default/css/*` correspondra aux fichiers ET à votre dossier,
ce qui bloquera l'installation.
Il faudra remplacer

<pre>'static/default/css/*'</pre>

par

<pre>'static/default/css/votre_dossier/*',t
'static/default/css/*.css'</pre>

Si vous n'avez modifié que des fichiers statiques, ou des templates, cela suffit.
Si vous avez modifié du code python, il faudra relancer uwsgi pour que
les modifications soient prises en compte :

<pre>sudo service uwsgi restart</pre>

N'oubliez pas de partager vos modifications avec la communauté !

<i class="glyphicon glyphicon-tree-deciduous" aria-hidden="true"></i>

 [1]: https://framabee.org
 [2]: https://github.com/asciimoo/searx
 [3]: https://virtualenv.pypa.io/en/latest/
 [4]: https://github.com/asciimoo/searx/wiki/Installation