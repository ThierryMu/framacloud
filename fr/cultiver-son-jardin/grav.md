# Installation de Grav

## Introduction

Grav est le logiciel utilisé pour créer des sites sur [frama.site](https://frama.site/).

<p class="alert alert-info">
  <b>Ce tutoriel est en cours de rédaction</b>.
  <br />
  Dans la suite de ce tutoriel, les instructions seront données pour un serveur dédié sous Debian Stretch, php 7.0 et un serveur web Nginx.
</p>

## Pré-requis

* PHP 5.5.9 ou supérieur. Normalement, les [modules suivants](https://learn.getgrav.org/basics/requirements#php-requirements) sont déjà installés sur votre serveur.
* git

`$ apt install php7.0 php7.0-gd php7.0-mbstring php7.0-curl php7.0-xml php7.0-zip git`

## Installation

### Semer (installation "from scratch")

Clonez le dépôt de Grav :

```
$ cd /var/www/html
$ git clone https://github.com/getgrav/grav.git
$ cd grav
$ bin/grav install
$ cd /var/www/html
$ chown -R www-data grav
```

### Migrer (à partir d’une sauvegarde .zip d’un site Framasite)

Obtenir des certificats LetsEncrypt associés au nouveau site (ci-après désigné par  monsite.mondomaine) : 
Taper dans le serveur dans lequel on migre les commandes suivantes (en root) :

```
systemctl stop apache2
certbot certonly --standalone -d monsite.mondomaine --agree-tos -m monmail@monadressemail
systemctl start apache2
```

(lire avant, si possible, les Terms of Service de LetsEncrypt)

Important : par la suite, pour renouveler les certificats, taper les commandes (en root) :

```
systemctl stop apache2
certbot renew
systemctl start apache2
```

Normalement, pour ça, on est prévenu·e par mail.

Récupérer la sauvegarde du site (au format zip ) dans la page d'administration du framasite à migrer.
Extraire la sauvegarde dans le dossier (à priori vide) du nouveau site (par exemple /var/www/html/monsite.mondomaine).
Créer un fichier de config spécifique au site (par exemple monsite.mondomaine.conf) et le mettre dans /etc/apache2/sites-available.

Écrire dans le fichier : 

```
<VirtualHost *:443>
                ServerName  monsite.mondomaine
                DocumentRoot /var/www/html/monsite.mondomaine
                SSLEngine		on
                SSLProtocol		all -SSLv3 -TLSv1 -TLSv1.1
                SSLCipherSuite	ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384
                SSLHonorCipherOrder	on
                SSLCompression	off
                SSLSessionTickets	off
                Header			always set Strict-Transport-Security "max-age=15768000; includeSubDomains; preload"
                SSLCertificateFile    "/etc/letsencrypt/live/monsite.mondomaine/fullchain.pem"
                SSLCertificateKeyFile   "/etc/letsencrypt/live/monsite.mondomaine/privkey.pem"
</VirtualHost>

<Directory "/var/www/html/monsite.mondomaine">
               AllowOverride All
</Directory>
```

Sauvegarder.

Taper la commande : `a2ensite monsite.mondomaine`
puis : `systemctl reload apache2`

Aller sur le nouveau site via son navigateur web préféré (en https) ; dans certains cas le premier chargement de la page peut prendre longtemps (plus de 10 minutes sur un serveur ARM (Lime 1 A20) qui à 512M de RAM).

