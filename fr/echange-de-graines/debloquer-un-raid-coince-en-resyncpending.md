# Débloquer un RAID coincé en resync=PENDING

Il nous arrive des fois que certains de nos disques RAID soient bloqués en `resync=PENDING`, ce qui est détecté par notre supervision, mais que l'on peut voir via un `cat /proc/mdstat` :

    md0 : active (auto-read-only) raid1 sda1[0] sdb1[1]
          16760832 blocks super 1.2 [2/2] [UU]
            resync=PENDING


Pour décoincer ça, il suffit de faire `mdadm --readwrite /dev/md0` :-)