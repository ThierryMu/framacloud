#!/bin/bash

SCRIPT=$(readlink -f $0)
SCRIPTPATH=`dirname $SCRIPT`

declare -A pages=(
    ['nextcloud']="installation-de-nextcloud"
    ['wallabag']="installation-de-wallabag"
    ['searx']="installation-de-searx"
    ['zerobin']="installation-de-zerobin"
    ['kanboard']="installation-de-kanboard"
    ['bicbucstriim']="installation-d-un-serveur-opds"
    ['umap']="installation-de-umap"
    ['framadate']="installation-de-framadate"
    ['lufi']="installation-de-lufi"
    ['framaforms']="installation-de-framaforms"
    ['gitlab']="installation-de-gitlab-et-mattermost"
    ['lstu']="installation-de-lstu"
    ['sympa']="installation-de-sympa"
    ['framaestro']="installation-de-framaestro"
    ['scrumblr']="installation-de-scrumblr"
    ['wisemapping']="installation-de-wisemapping"
    ['minetest']="installation-de-minetest"
    ['ttrss']="installation-de-tinytinyrss"
    ['turtl']="installation-de-turtl"
    ['etherpad']="installation-detherpad"
    ['mastodon']="installation-de-mastodon"
    ['lutim']="installation-de-lutim"
    ['jitsi-meet']="installation-de-jitsi-meet"
    ['svg-edit']="installation-de-svg-edit"
    ['loomio']="installation-de-loomio"
    ['shaarli']="installation-de-shaarli"
    ['grav']="grav.html"
    ['grav']="installation-de-grav"
)

cd $SCRIPTPATH

mkdir _book/cultiver-son-jardin/

for k in "${!pages[@]}"; do
    mkdir _book/cultiver-son-jardin/${pages[$k]}
    echo '<html><head><meta http-equiv="refresh" content="0; URL=../../fr/cultiver-son-jardin/'$k'.html"></head><body></body></html>' > _book/cultiver-son-jardin/${pages[$k]}/index.html
done
